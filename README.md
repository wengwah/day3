-----------------------------------
Steps to prepare files for the day
-----------------------------------

##1. Prepare Server folder

##2. Prepare Client Folder

##3. Prepare Git 
####3.1 Initialize git on folder ```git init``` 
####3.2 Create remote repository in bitbucket (Plus sign on bitbucket)
####3.3 Add remote add origin by pasting over code ```git remote add origin (http://bitbucketlink here)```
####3.4 Git add to staging area ```git add .```
####3.5 Git commit to local repository ```git commit -m "message here"```
####3.6 Git push to remote repository ```git push origin master -u```

##4. Prepare Dependencies
####4.1 Prepare scafolding aka package.JSON file ```npm init```
####4.2 Download and link relevant node_modules ```npm install express --save```
####4.3 Download bower globally with -g (This handles front end modules and dependencies) ```npm install -g bower```

##5. Starting the app (Console)
####5.1 ```export NODE_PORT=3000```
####5.2 ```nodemon``` or ```nodemon server/app.js```

##6. Launching the app on the browser
####6.1 ```http://localhost:3000```
